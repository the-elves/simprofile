#include <stdio.h>

int dummy() {
	int i, j = 0;
	int flag_1 = 0, flag_2 = 0;
__asm__("addu/a $6, $6, 0");
	for(i = 300000; i > 0; i--) {
		j++;
		if(flag_1 < 500)
			flag_1++;
		else if(!flag_2) {
			__asm__("addu/a $6, $6, 0");
			flag_2 = 1;
		}
	}
	return j;
}

int main(){
	int i, j = 0;
	int flag_1 = 0, flag_2 = 0;
__asm__("addu/a $6, $6, 0");
	for( i = 0 ; i <100000; i++){
		j++;
		if(flag_1 < 500)
			flag_1++;
		else if(!flag_2) {
			__asm__("addu/a $6, $6, 0");
			flag_2 = 1;
		} 	
	}
	i = dummy();
	printf("%d",i);
	return 0;
}
